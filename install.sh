name=".battify"
cp battify ~/$name
chmod 744 ~/$name

systemd_config_dir=".config/systemd/user"
service_filename="battify.service"

cd

if [ ! -d $systemd_config_dir ]; then
    mkdir -p $systemd_config_dir
fi

cd $systemd_config_dir

if [ -f $service_filename ] ; then
	rm $service_filename
fi

touch $service_filename
my_username=$(whoami)
echo "Generating service file for: $my_username"

cd ~/.config/systemd/user/
touch $service_filename

echo "[Unit]" >> $service_filename
echo "Description=Critical battery notification service" >> $service_filename

echo "[Service]" >> $service_filename
echo "ExecStart=/home/$my_username/$name" >> $service_filename

echo "[Install]" >> $service_filename
echo "WantedBy=default.target" >> $service_filename

systemctl --user daemon-reload
systemctl --user stop $service_filename
systemctl --user start $service_filename
systemctl --user enable $service_filename
systemctl --user status $service_filename
